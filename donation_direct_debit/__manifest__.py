# Copyright 2015-2021 Akretion France (http://www.akretion.com/)
# @author: Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).


{
    "name": "Donation Direct Debit",
    "version": "2.0.1.0.0",
    "category": "Accounting",
    "license": "AGPL-3",
    "summary": "Auto-generate direct debit order on donation validation",
    "author": "Akretion, Odoo Community Association (OCA)",
    "maintainers": ["alexis-via"],
    "website": "https://gitlab.com/flectra-community/donation",
    "depends": ["account_banking_sepa_direct_debit", "donation"],
    "data": [
        "views/donation.xml",
    ],
    "demo": ["demo/donation_demo.xml"],
    "installable": True,
}
