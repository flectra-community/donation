# Flectra Community / donation

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[donation](donation/) | 2.0.1.1.0| Manage donations
[donation_recurring](donation_recurring/) | 2.0.1.0.0| Manage recurring donations
[product_analytic_donation](product_analytic_donation/) | 2.0.1.0.0| Glue module between donation and product_analytic
[donation_base](donation_base/) | 2.0.1.1.0| Base module for donations
[donation_sale](donation_sale/) | 2.0.1.0.0| Manage donations in sale orders
[donation_direct_debit](donation_direct_debit/) | 2.0.1.0.0| Auto-generate direct debit order on donation validation


