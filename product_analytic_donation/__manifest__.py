# Copyright 2017-2021 Akretion (http://www.akretion.com/)
# @author: Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Product Analytic Donation",
    "version": "2.0.1.0.0",
    "category": "Donation",
    "license": "AGPL-3",
    "summary": "Glue module between donation and product_analytic",
    "author": "Akretion, Odoo Community Association (OCA)",
    "maintainers": ["alexis-via"],
    "website": "https://gitlab.com/flectra-community/donation",
    "depends": ["donation", "product_analytic"],
    "auto_install": True,
    "installable": True,
}
